import 'package:get/get.dart';
import 'package:untitled4/Controllers/HomeController.dart';
import 'package:untitled4/Models/movie.dart';

final homectrlr = Get.put(HomeController());

class DetailsController extends GetxController {
  List<Movie> moviesList = [];
  String movieID = "";
  Movie neededMovie = new Movie();
  String directorOneString = "";
  String castOneString = "";

  @override
  void onInit() {
    super.onInit();
    moviesList = homectrlr.movies;
    movieID = homectrlr.detailedMovieID;
    moviesList.forEach((element) {
      if (element.id == movieID) {
        neededMovie = element;
      }
    });

    print("*********From Details Controller******");
    print("Movies List :");
    print(moviesList);
    print(neededMovie.overview);
    print(movieID);

    neededMovie.director!.forEach((element) {
      directorOneString += element + ",";
    });

    neededMovie.cast!.forEach((element) {
      castOneString += element + ",";
    });
  }
}
