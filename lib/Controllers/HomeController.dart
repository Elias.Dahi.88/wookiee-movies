import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:untitled4/Models/API_Response.dart';
import '../Models/movie.dart';
import 'package:untitled4/network/DataLoader.dart' as DataLoader;

class HomeController extends GetxController {
  TextEditingController search = TextEditingController();
  List<Movie> movies = [];
  List<String> duplicatesGenre = [];
  List<String> genre = [];
  String detailedMovieID = "";
  API_Response result = new API_Response();

  RxBool isAsync = false.obs;
  RxBool error = false.obs;

  Future<void> getData() async {
    isAsync.value = true;

    result = await DataLoader.getRequest(
      url: DataLoader.baseUrl,
      timeout: 15,
      ctx: Get.context,
    );

    result.movies!.forEach((element) {
      movies.add(Movie.fromJson(element));
    });

    movies.forEach((element) {
      element.generes!.forEach((element2) {
        duplicatesGenre.add(element2);
      });
    });

    genre = duplicatesGenre.toSet().toList();

    isAsync.value = false;
  }

  @override
  void onInit() async {
    super.onInit();
    await getData();
    print(genre.length);
    print(movies.length);
  }
}
