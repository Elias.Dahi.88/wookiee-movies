import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:untitled4/Views/DetailsView.dart';
import 'package:untitled4/Views/HomePage.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      debugShowCheckedModeBanner: false,
      getPages: [
        GetPage(name: "/", page: () => HomePage()),
        GetPage(name: "/details", page: () => DetailsView()),
      ],
    );
  }
}
