import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:untitled4/Models/API_Response.dart';
import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' as http;

const String baseUrl = 'https://wookie.codesubmit.io/movies';
const String searchURL = "$baseUrl?q=";

const String token = 'Wookie2021';

Future<API_Response> postRequest(
    {String? url,
    int? timeout,
    BuildContext? ctx,
    dynamic body,
    Map<String, String> headers = const {
      'Content-Type': 'application/x-www-form-urlencoded',
      'Authorization': "Bearer Wookie2019"
    }}) async {
  Uri parsedUrl = Uri.parse(url!);
  try {
    print('************ body request ***********');
    print(body.runtimeType);
    print(headers);
    final response = await http
        .post(parsedUrl, body: body, headers: headers)
        .timeout(Duration(seconds: timeout!));
    if (response.statusCode == 200) {
      return API_Response.fromJson(jsonDecode(response.body));
    } else {
      print(response.statusCode);
      return API_Response.fromJson(
          {"code": "-999", "message": "حدث خطأ في الخادم يرجى المحاولة لاحقاً", "data": null});
    }
  } on SocketException catch (e) {
    return API_Response.fromJson(
        {"code": "-999", "message": "يرجى التأكد من اتصالك بالانترنيت", "data": null});
  } on TimeoutException catch (e) {
    return API_Response.fromJson(
        {"code": "-999", "message": "لقد انتهى وقت الاتصال، يرجى المحاولة مرة أخرى", "data": null});
  } on Exception catch (e) {
    return API_Response.fromJson(
        {"code": "-99999999999999999999", "message": e.toString(), "data": null});
  }
}

Future<API_Response> getRequest(
    {String? url,
    int? timeout,
    BuildContext? ctx,
    Map<String, String> headers = const {
      'Content-Type': 'application/x-www-form-urlencoded',
      'Authorization': "Bearer Wookie2019"
    }}) async {
  Uri parsedUrl = Uri.parse(url!);
  try {
    print('************ body request ***********');
    print(url);
    print(headers);
    final response =
        await http.get(parsedUrl, headers: headers).timeout(Duration(seconds: timeout!));
    if (response.statusCode == 200) {
      return API_Response.fromJson(jsonDecode(response.body));
    } else {
      print(response.statusCode);
      return API_Response.fromJson(
          {"code": "-999", "message": "Something went wrong", "data": null});
    }
  } on SocketException catch (e) {
    return API_Response.fromJson(
        {"code": "-999", "message": "يرجى التأكد من اتصالك بالانترنيت", "data": null});
  } on TimeoutException catch (e) {
    return API_Response.fromJson(
        {"code": "-999", "message": "لقد انتهى وقت الاتصال، يرجى المحاولة مرة أخرى", "data": null});
  } on Exception catch (e) {
    return API_Response.fromJson(
        {"code": "-99999999999999999999", "message": e.toString(), "data": null});
  }
}
