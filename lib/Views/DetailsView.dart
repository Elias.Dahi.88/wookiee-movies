import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:untitled4/Controllers/DetailsController.dart';
import 'package:get/get.dart';

class DetailsView extends StatelessWidget {
  double rating = 0;
  TextEditingController search = TextEditingController();
  Icon cusIcon = const Icon(Icons.search);
  Widget cusSearcher = const Text("AppBar");
  final detailsctrlr = Get.put(DetailsController());

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    return Scaffold(
      appBar: AppBar(
        title: const Text("Wookie Movies"),
        backgroundColor: Colors.red,
        actions: <Widget>[
          IconButton(
            onPressed: () {
              if (cusIcon.icon == Icons.search) {
                cusIcon = const Icon(Icons.cancel);
                cusSearcher = TextField(
                  controller: search,
                );
              } else {
                cusIcon = const Icon(Icons.search);
                cusSearcher = const Text("Appbar");
              }
            },
            icon: cusIcon,
          )
        ],
      ),
      body: SizedBox(
        width: 500.0,
        child: Row(
          children: [
            Column(
              children: [
                Image.network(
                  detailsctrlr.neededMovie.poster!,
                  height: 200.0,
                ),
              ],
            ),
            SizedBox(
              width: width * 0.6,
              child: Column(
                children: [
                  Text(
                    detailsctrlr.neededMovie.title!,
                    style: const TextStyle(
                      fontSize: 12,
                      fontWeight: FontWeight.bold,
                      color: Colors.black,
                    ),
                  ),
                  RatingBar.builder(
                    initialRating: double.parse(detailsctrlr.neededMovie.imdb_rating!),
                    minRating: 1,
                    direction: Axis.horizontal,
                    allowHalfRating: true,
                    itemCount: 10,
                    itemPadding: const EdgeInsets.symmetric(horizontal: 4.0),
                    itemBuilder: (context, _) => const Icon(
                      Icons.star,
                      color: Colors.amber,
                    ),
                    onRatingUpdate: (rating) {
                      print(rating);
                    },
                  ),
                  FittedBox(
                    child: Row(
                      children: [
                        Text(
                          detailsctrlr.neededMovie.released_on!,
                          style: const TextStyle(
                            fontSize: 12,
                            fontWeight: FontWeight.bold,
                            color: Colors.black,
                          ),
                        ),
                        Text(" | "),
                        Text(
                          detailsctrlr.neededMovie.length!,
                          style: const TextStyle(
                            fontSize: 12,
                            fontWeight: FontWeight.bold,
                            color: Colors.black,
                          ),
                        ),
                        Text(" | "),
                        Text(
                          detailsctrlr.directorOneString,
                          style: const TextStyle(
                            fontSize: 12,
                            fontWeight: FontWeight.bold,
                            color: Colors.black,
                          ),
                        ),
                      ],
                    ),
                  ),
                  Text("Cast: " + detailsctrlr.castOneString,
                      style: const TextStyle(
                        fontSize: 12,
                        fontWeight: FontWeight.bold,
                        color: Colors.black,
                      )),
                  Text(
                    detailsctrlr.neededMovie.overview!,
                    overflow: TextOverflow.visible,
                    style: const TextStyle(
                      fontSize: 12,
                      fontWeight: FontWeight.bold,
                      color: Colors.black,
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
