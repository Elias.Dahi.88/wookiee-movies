import 'dart:io';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:modal_progress_hud_nsn/modal_progress_hud_nsn.dart';
import 'package:untitled4/Controllers/DetailsController.dart';
import 'package:untitled4/Controllers/HomeController.dart';
import 'package:get/get.dart';
import 'package:cached_network_image/cached_network_image.dart';

final homectrlr = Get.put(HomeController());
final detailsctrlr = Get.put(DetailsController());

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.red,
        title: const Text(
          "Wookiee Movies",
          style: TextStyle(color: Colors.white),
        ),
        centerTitle: false,
      ),
      backgroundColor: Colors.white,
      body: Obx(
        () => ModalProgressHUD(
          inAsyncCall: homectrlr.isAsync.value,
          progressIndicator: (Platform.isAndroid)
              ? const CircularProgressIndicator()
              : const CupertinoActivityIndicator(
                  radius: 15.0,
                ),
          dismissible: false,
          child: Flex(
            direction: Axis.vertical,
            children: [
              Expanded(
                child: ListView.builder(
                  scrollDirection: Axis.vertical,
                  itemCount: homectrlr.genre.length,
                  itemBuilder: (BuildContext context, int index1) {
                    return SizedBox(
                      height: 300.0,
                      child: Column(
                        children: [
                          Text(
                            homectrlr.genre[index1],
                            style: const TextStyle(
                              fontSize: 18.0,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          Expanded(
                            child: ListView.builder(
                              scrollDirection: Axis.horizontal,
                              itemCount: homectrlr.movies.length,
                              itemBuilder: (BuildContext context, int index2) {
                                if (homectrlr.movies[index2].generes!
                                    .contains(homectrlr.genre[index1])) {
                                  return Padding(
                                    padding: const EdgeInsets.symmetric(horizontal: 8.0),
                                    child: GestureDetector(
                                      onTap: () {
                                        homectrlr.detailedMovieID = homectrlr.movies[index2].id!;
                                        Get.toNamed("/details");
                                      },
                                      child: Container(
                                        width: 150.0,
                                        height: 50.0,
                                        child: CachedNetworkImage(
                                          imageUrl: homectrlr.movies[index2].poster!,
                                        ),
                                      ),
                                    ),
                                  );
                                } else {
                                  return Container();
                                }
                              },
                            ),
                          ),
                        ],
                      ),
                    );
                  },
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
