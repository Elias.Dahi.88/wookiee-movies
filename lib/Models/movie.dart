class Movie {
  String? backdrop; //link
  List<String>? cast; // list of strins etrka string ?
  String? classifaction;
  List<String>? director;
  List<String>? generes; // list of strins etrka string ?
  String? id;
  String? imdb_rating;
  String? overview;
  String? length;
  String? poster; // link
  String? released_on;
  String? slug;
  String? title;

  Movie({
    this.backdrop,
    this.cast,
    this.classifaction,
    this.director,
    this.generes,
    this.id,
    this.imdb_rating,
    this.overview,
    this.length,
    this.poster,
    this.released_on,
    this.slug,
    this.title,
  });

  factory Movie.fromJson(Map<String, dynamic> json) {
    List<String> director = [];
    if (json['director'] is String) {
      director.add(json['director']);
    } else {
      director.addAll(List<String>.from(json['director']));
    }
    return Movie(
        backdrop: json['backdrop'],
        cast: List<String>.from(json['cast']),
        classifaction: json['classifaction'],
        director: director,
        generes: List<String>.from(json['genres']),
        id: json['id'],
        imdb_rating: json['imdb_rating'].toString(),
        overview: json['overview'],
        length: json['length'],
        poster: json['poster'],
        released_on: json['released_on'],
        slug: json['slug'],
        title: json['title']);
  }
}
