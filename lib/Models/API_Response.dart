// ignore: camel_case_types
import 'package:untitled4/Models/movie.dart';

class API_Response {
  List<dynamic>? movies;

  API_Response({this.movies});

  factory API_Response.fromJson(dynamic json) {
    return API_Response(movies: json['movies']);
  }
}
